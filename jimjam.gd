@tool
extends Node2D

## The only difference was to move the script one node down (so it's not on the root)
## so that the script applies to *this* scene instance and not the one that *this* scene
## is instanced into. Notice how there's no script on the jimjamsc1 node in jimjam.tscene

@export var spriteNodes: Array

func _ready() -> void:

	spriteNodes.clear()

	var _children := get_children()
	if _children:
		for _child in _children:
			# Rename to prevent the replacement node below having a clash
			_child.name = _child.name+"_XX"
			_child.queue_free()

	var pos:Vector2 = Vector2.ZERO
	for i in 2:
		var _node: Sprite2D = Sprite2D.new()
		_node.name = "Node"+str(i)
		_node.texture = load("res://icon.svg")
		_node.position = pos
		pos.x += 50
		pos.y += 50
		add_child(_node)

		_node.owner = get_tree().edited_scene_root
		#_node.set_owner(self.owner) #<-- I prefer this style of set_owner

		# Can't store nodes in a dictionary via tool script, so needs to be a path to a node
		spriteNodes.append(_node.get_path())

		$"../AnimationPlayer".play("wobble")
